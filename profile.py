"""Allocate an FE and run the monitor. """


# Import the emulab extensions library.
import geni.rspec.emulab
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab.pnext as pn
import geni.rspec.igext as ig
import geni.rspec.emulab.spectrum as spectrum

NODE_IMAGE = \
        "urn:publicid:IDN+emulab.net+image+patwari:cir_localization"
setup_command = "/local/repository/startup.sh"

def b210_nuc_pair(idx, b210_node, NodeID):
    b210_nuc_pair_node = request.RawPC("b210-%s-%s"%(b210_node.aggregate_id,NodeID))
    agg_full_name = "urn:publicid:IDN+%s.powderwireless.net+authority+cm"%(b210_node.aggregate_id)
    b210_nuc_pair_node.component_manager_id = agg_full_name
    b210_nuc_pair_node.component_id = NodeID
    b210_nuc_pair_node.disk_image = NODE_IMAGE

    service_command = setup_command
    b210_nuc_pair_node.addService(rspec.Execute(shell="bash", command=service_command))



# Create a portal context.
pc = portal.Context()

# Create a Request object to start building the RSpec. 
request = pc.makeRequestRSpec()

# Node type parameter for PCs to be paired with B210 radios.
# Restricted to those that are known to work well with them.
pc.defineParameter(
    "NodeID",
    "Compute node",
    portal.ParameterType.STRING, "nuc2",
    ["nuc1","nuc2"],
    "Compute node to be paired with the X310 Radios",
)

fixed_endpoint_aggregates = [
    ("web", "WEB"),
    ("ebc", "EBC"),
    ("bookstore", "Bookstore"),
    ("humanities", "Humanities"),
    ("law73", "Law 73"),
    ("madsen", "Madsen"),
    ("sagepoint", "Sage Point"),
    ("moran", "Moran"),
    ("cpg", "Garage"),
    ("guesthouse", "Guesthouse")
]


pc.defineStructParameter("b210_nodes", "B210 Radios", [],
                                     multiValue=True,
                                     min=0, max=None,
                                     members=[
                                         portal.Parameter(
                                             "aggregate_id",
                                             "Fixed Endpoint B210",
                                             portal.ParameterType.STRING,
                                             fixed_endpoint_aggregates[0],
                                             fixed_endpoint_aggregates)
                                     ],
                                    )

# Frequency/spectrum parameters
pc.defineStructParameter(
    "freq_ranges", "Range", [],
    multiValue=True,
    min=1,
    multiValueTitle="Frequency ranges for over-the-air operation.",
    members=[
        portal.Parameter(
            "freq_min",
            "Frequency Min",
            portal.ParameterType.BANDWIDTH,
            2400,
            longDescription="Values are rounded to the nearest kilohertz."
        ),
        portal.Parameter(
            "freq_max",
            "Frequency Max",
            portal.ParameterType.BANDWIDTH,
            6000.0,
            longDescription="Values are rounded to the nearest kilohertz."
        ),
    ])

params = pc.bindParameters()

for i, frange in enumerate(params.freq_ranges):
    if frange.freq_min < 2400 or frange.freq_min > 6000 \
       or frange.freq_max < 2400 or frange.freq_max > 6000:
        perr = portal.ParameterError("Frequencies must be between 3400 and 3800 MHz", ["freq_ranges[%d].freq_min" % i, "freq_ranges[%d].freq_max" % i])
        portal.context.reportError(perr)
    if frange.freq_max - frange.freq_min < 1:
        perr = portal.ParameterError("Minimum and maximum frequencies must be separated by at least 1 MHz", ["freq_ranges[%d].freq_min" % i, "freq_ranges[%d].freq_max" % i])
        portal.context.reportError(perr)


pc.verifyParameters()

# Request frequency range(s)
for frange in params.freq_ranges:
    request.requestSpectrum(frange.freq_min, frange.freq_max, 100)

for i, b210_node in enumerate(params.b210_nodes):
    b210_nuc_pair(i, b210_node, params.NodeID)

# Final rspec.
pc.printRequestRSpec(request)
